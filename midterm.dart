import 'dart:io';
import 'dart:convert';

List token(String expr) {
  List separators = ["+", "-", "*", "/", "%", "^", "(", ")"];
  List num = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
  List result = [];
//remove space
  for (var i = 0; i < expr.length; i++) {
    if (expr[i] == ' ') {
      // add expr
      result.add(expr[i]);
      //remove expr to " "
      result.remove(expr[i]);
    } else {
      result.add(expr[i]);
    }
  }
  result = tokenizingNegative(result);
  return result;
}

// for negative values
List tokenizingNegative(List token) {
  for (var i = 0; i < token.length - 1; i++) {
    for (var j = i + 1; j < token.length; j++) {
      if (token[i] == "-" && token[j] == '(') {
        break;
      } else if (token[i] == '-') {
        var temp = token[j];
        token[i] = '-$temp';
        token.remove(token[j]);
      }
    }
  }
  return token;
}

//precedence operator
int checkPreLevel(String operator) {
  if (operator == "(" || operator == ")") {
    return 4;
  }
  if (operator == "^") {
    return 3;
  }
  if (operator == "*" || operator == "/") {
    return 2;
  }
  if (operator == "%") {
    return 3;
  }
  if (operator == "+" || operator == "-") {
    return 1;
  }
  return -1;
}

// check integer
bool isInteger(String int) {
  if (int == null) {
    return false;
  }
  return double.tryParse(int) != null;
}

List infToPost(List infix) {
  List Operator = [];
  List Postfix = [];
  infix.forEach((i) {
    // if is integer , add token
    if (isInteger(i)) {
      Postfix.add(i);
    }
    //
    else if (isInteger(i) == false && i != '(' && i != ')') {
      //operators is not empty and last operator is not (
      //and precedence(token) < precedence(last item in operators)
      // Remove the last item from operators and add it to postfix Add token to the end of operators
      while (Operator.isNotEmpty &&
          Operator.last != '(' &&
          checkPreLevel(i) < checkPreLevel(Operator.last)) {
        Postfix.add(Operator.removeLast());
      }
      Operator.add(i);
    }
    // If the token is (
    // Add token to the end
    else if (i == '(') {
      Operator.add(i);
    }
    // operators If the token is )
    else if (i == ')') {
      //While the last item in operators is not (
      //Remove the last item from operators
      //add it to postfix Remove the open parenthesis from operators
      while (Operator.last != '(') {
        Postfix.add(Operator.removeLast());
      }
      Operator.remove('(');
    }
  });
  //operators is not the empty
  //Remove the last item from operators
  //add it to postfix
  //Return postfix as the result
  while (Operator.isNotEmpty) {
    Postfix.add(Operator.removeLast());
  }
  return Postfix;
}

double evaluatePost(List postfix) {
  List value = [];
  double left, right;

// Convert it to an integer and add it to the end of values
//If the token is a number
  postfix.forEach((i) {
    if (isInteger(i)) {
      double temp = double.parse(i);
      value.add(temp);
    }
    //Remove an item from the end of values and call it right
    //Remove an item from the end of values and call it left
    //Append the result to the end of values by calculator
    //Return the first item
    else {
      right = value.removeLast();
      left = value.removeLast();
      var result = calculator(left, right, i);
      value.add(result);
    }
  });
  return value.first;
}

//calculator , result
calculator(double left, double right, String operator) {
  double result = 0;
  switch (operator) {
    case "+":
      {
        result = left + right;
      }
      break;
    case "-":
      {
        result = left - right;
      }
      break;
    case "*":
      {
        result = left * right;
      }
      break;
    case "/":
      {
        result = left / right;
      }
      break;
    case "%":
      {
        result = left % right;
      }
      break;
  }
  return result;
}

void main(List<String> args) {
  print('Input mathematical experession');
  var expr = stdin.readLineSync()!;
  var tokens = token(expr);
  print('infix: $tokens');
  var postfix = infToPost(infToPost(tokens));
  print('postfix: $postfix');
  var result = evaluatePost(postfix);
  print('result evaluate postfix: $result');
}
